package com.jerimkaura.oasis.web.models.requests;

import lombok.Data;

@Data
public class ForgotPasswordRequest {
    private String username;
}
